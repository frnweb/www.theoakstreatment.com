<?php

function frn_infinity_analytic_options() {
	add_settings_field('infinity_dest', "Discovery Numbers <span style='white-space:nowrap;'>Used <a href='javascript:showhide(\"frn_plugin_infinity_dest\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_admin_infinity_discovery', 'frn_tracking', 'tracking_section');
	add_settings_field('infinity_js', "Analytics <span style='white-space:nowrap;'>Options <a href='javascript:showhide(\"frn_plugin_infinity_js\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_admin_infinity_js', 'frn_tracking', 'tracking_section');
	add_settings_field('infinity_format', "Phone Number <span style='white-space:nowrap;'>Format <a href='javascript:showhide(\"frn_plugin_infinity_format\")' ><img src='".$GLOBALS['help_image']."' /></a>", 'frn_admin_infinity_format', 'frn_tracking', 'tracking_section');
	add_settings_field('infinity_pools', "Infinity <span style='white-space:nowrap;'>Pools <a href='javascript:showhide(\"frn_plugin_infinity_pools\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_admin_infinity_pools', 'frn_tracking', 'tracking_section');
	add_settings_field('infinity_tags', "Infinity URL <span style='white-space:nowrap;'>Tags <a href='javascript:showhide(\"frn_plugin_infinity_tags\")' ><img src='".$GLOBALS['help_image']."' /></a></span>", 'frn_admin_infinity_tags', 'frn_tracking', 'tracking_section');
} 

function frn_admin_infinity_main() {
	$phones = get_option('phones'); 
	$phones_main_pool=""; //default FRN pool
	if(isset($phones['infinity']['main_pool'])) $phones_main_pool=$phones['infinity']['main_pool']; ?>
	<select id='main_pools_list' name='phones[infinity][main_pool]'>
		<option value=""></option><?php
		$domain=home_url();
		$infinity_pools="";
		if(function_exists('frn_infinity_pools_list')) {
			$infinity_pools=frn_infinity_pools_list('no_tags'); //get standard list of tag-only pools
		}
		if($infinity_pools!=="") {
			foreach($infinity_pools as $key => $pool) { 
				$selected=""; 
				//if(!stripos($pool['name'],"local")!==false) { //removes the url triggered local numbers from the list (changed direction by removing anything that isn't a main pool using "no_tags" above)
					if($phones_main_pool!=="") {
						if($key==$phones_main_pool) $selected="selected "; //if options saved in DB, overrides anything automatic
					}
					elseif(stripos($domain,$pool['kw'])!==false) $selected="selected "; 
				?>
		<option value="<?=$key;?>" <?=$selected;?>><?=$pool['name'];?></option><?php 
				//}
			}
			} ?>
	</select>
	<?php 
}

function frn_admin_infinity_act($phones="") {
	//in frn_tracking_services function in admin_submenu_phone.php
	$activate_chkd="";
	$section_id_dt='dt_options'; 
	$section_id_inf='inf_options';
	$section_id_none='no_service';
	$section_id_dest='destinations'; 
	if(!is_array($phones)) $phones = get_option('phones');  //if $phones was not passed through
	if(!isset($phones['tracking'])) $phones['tracking']="";
	if(!isset($phones['infinity']['activate'])) $phones['infinity']['activate']="";
	$activate=$phones['infinity']['activate'];
	if($activate=="none") frn_admin_infinity_hide(array(4,5,6,8)); //hide infinity options
	?>
	<div id="inf_options" style="display:<?=(($phones['tracking']=='infinity' || $phones['tracking']=='dt-ads') ? 'block' : 'none' );?>" >
		<input type="radio" name="phones[infinity][activate]" value="all"  <?=(($activate=='all') ? 'checked="checked"' : '' );?> 
			onClick="hide_infinity('table-row',[3,4,5,7,8]);showhide('<?=$section_id_dt;?>','none');showhide('<?=$section_id_inf;?>','block');showhide('<?=$section_id_none;?>','none');" 
			/> Activate all Infinty features<br />
		<input type="radio" name="phones[infinity][activate]" value="js"   <?=(($activate=='js')  ? 'checked="checked"' : '' );?> 
			onClick="hide_infinity('table-row',[3,4,5,7,8]);showhide('<?=$section_id_dt;?>','none');showhide('<?=$section_id_inf;?>','block');showhide('<?=$section_id_none;?>','none');" 
			/> Disable Infinity pools<br />
		<input type="radio" name="phones[infinity][activate]" value="none" <?=(($activate=='none' || $activate=='') ? 'checked="checked"' : '' );?> 
			onClick="hide_infinity('none',[3,4,5,7,8]);showhide('<?=$section_id_dt;?>','none');" 
			/> Disable the Infinity system (Talkdesk numbers still active)
	</div>
	<div id="frn_plugin_infinity_help" class="frn_help_boxes">
		<ul class="frn_level_1">
			<li><b>Activate All:</b> This activates web tracking, chat/email conversion tracking, and using Infinity's pool tracking numbers with the shortcodes.</li>
			<li><b>Disable Pools:</b> This turns off using Infinity's auto discovery numbers. It defaults to the main number entered above. Web tracking and chat/email conversions are still on.</li>
			<li><b>Disable System:</b> This turns off all Infinity features. Links around numbers will have Infinty class and data removed.</li>
			<li><b>IMPORTANT: </b>
				<ul>
					<li>Talkdesk destination numbers will remain even if Infinity system disabled.</li>
					<li>The assumption is that Talkdesk remains active, but Infinity isn't.</li>
					<li>To revert all phone number shortcodes to the main number (in case both systems go down), you must choose "none" for tracking services.</li>
					<li>Old numbers will also be replaced with Talkdesk numbers unless "none" is selected.</li>
				</ul>
			</li>
		</ul>
	</div>
	<?php
}

function frn_admin_infinity_discovery() {
	$phones = get_option('phones');  //if $phones was not passed through

	//Set defaults for first install
	$phones_discovery="td"; //default for after launch
	$timezone = 'America/Chicago';
	$now = new DateTime('now', new DateTimeZone($timezone));
	$launch_time = new DateTime("2019-03-02 00:00:00", new DateTimeZone($timezone));
	if($now<$launch_time) {
		$phones_discovery="mitel"; //default for pre-launch
	}
	//end defaults

	if(isset($phones['infinity']['discovery'])) $phones_discovery = $phones['infinity']['discovery'];
	?>
	<div id="discovery_numbers" style="display:block;" >
		<input type="radio" name="phones[infinity][discovery]" value="td"    <?=(($phones_discovery=='td' || $phones_discovery=='')  ? 'checked="checked"' : '' );?> /> Talkdesk<br />
		<input type="radio" name="phones[infinity][discovery]" value="mitel" <?=(($phones_discovery=='mitel') ? 'checked="checked"' : '' );?> /> Original Toll-Free
	</div>
	<div id="frn_plugin_infinity_dest" class="frn_help_boxes">
		<ul class="frn_level_1">
			<li>This is only helpful prior to Talkdesk launch.</li>
			<li>The numbers that this system delivers in HTML pages need to match the numbers Google My Business has as our main numbers or that system will reject the Infinity numbers we give it.</li>
			<li><b>Talkdesk:</b> This replaces all numbers and phone shortcodes with Infinity's "discovery" numbers so that the right facility pool is triggered that sends calls to Talkdesk.</li>
			<li><b>Original Toll-Free:</b> Prior to launching Talkdesk in late February 2019, we used toll-free numbers that have been since ported to Infinity. For GMB to work, the numbers this system delivers is the normal Web Local numbers we have always used there prior to Talkdesk. It will NOT be our typical Web FRN, Web Bear, etc. set of numbers.</li>
		</ul>
	</div>
	<?php
}

function frn_admin_infinity_js(){
	//in main admin add_settings_field function in admin_submenu_phone.php
	$phones=get_option('phones');
	if(!isset($phones['tracking'])) $phones['tracking']="";
	if(!isset($phones['infinity']['js'])) $phones['infinity']['js']="";
	if(!isset($phones['infinity']['js']['ga']))  $ga="";
		else $ga=$phones['infinity']['js']['ga'];
	if(!isset($phones['infinity']['js']['vwo'])) $vwo="";
		else $vwo=$phones['infinity']['js']['vwo'];
	if(!isset($phones['infinity']['js']['dc']))  $dc="";
		else $dc=$phones['infinity']['js']['dc'];
	//style="display:<?=(($phones['tracking']=='infinity' || $phones['tracking']=='dt-ads') ? 'block' : 'none' );
	?>
	<div id="inf_js"  >
		<input type="checkbox" name="phones[infinity][js][ga]"  value="yes" <?=(($ga!=='')   ? 'checked="checked"' : '' );?> /> Google Analytics<br />
		<input type="checkbox" name="phones[infinity][js][vwo]" value="yes" <?=(($vwo!=='')  ? 'checked="checked"' : '' );?> /> Visual Web Optimizer<br />
		<input type="checkbox" name="phones[infinity][js][dc]"  value="yes" <?=(($dc!=='')   ? 'checked="checked"' : '' );?> /> DoubleClick Campaign Manager<br />
	</div>
	<div id="frn_plugin_infinity_js" class="frn_help_boxes">
		<ul class="frn_level_1">
			<li>Google Analytics should be activated by default. Infinity won't report activity to GA or be able to tie activity back to Google Ads without it being included.</li>
			<li>All tools selected here also need to be set up in the Infinity system before user activity will be reported from Infinity.</li>
		</ul>
	</div>
	<?php 
}

function frn_admin_infinity_format() {
	$phones = get_option('phones');
	if(!isset($phones['infinity']['format'])) $phones['infinity']['format']=""; 
	$format=$phones['infinity']['format'];?>
	<select id='inf_phone_format' name='phones[infinity][format]'>
		<option value="-"   <?=($format=="-" || $format=="")   ? 'selected':'';?> >###-###-####</option>
		<option value="()"  <?=($format=="()")  ? 'selected':'';?> >(###) ###-####</option>
		<option value="."   <?=($format==".")   ? 'selected':'';?> >###.###.####</option>
		<!--
		Infinity's system cannot discover a number if it uses these formats.
		<option value="1()" <?=($format=="1()") ? 'selected':'';?> >1 (###) ###-####</option>
		<option value="1-"  <?=($format=="1-")  ? 'selected':'';?> >1-###-###-####</option>
		<option value="1."  <?=($format=="1.")  ? 'selected':'';?> >1.###.###.####</option>-->
	</select>
	<div id="frn_plugin_infinity_format" class="frn_help_boxes">
		<ul class="frn_level_1">
			<li>The system only has phone number digits. Without this, they wouldn't look like a phone number.</li>
			<li>If you want a "1" prior to the number, it'll need to be outside of the shortcode. Infinity's system will not recognize a number if it's included.</li>
			<li>People will see discovery numbers prior to them changing or if JS is turned off or Infnity's system goes down.</li>
			<li>This option only controls what that discovery number looks like.</li>
			<li>To change the format of the Infinity number, we will have to change that in the platform's settings at infinity.co. The plugin can't influence what happens after the number is replaced.</li>
		</ul>
	</div>
	<?php 
}

function frn_admin_infinity_hide($hide_rows="") {
	//Referred to above in frn_infinity_activate function
	//This hides these respective rows in the admin table that relate to infinity settings if infinity isn't activated
	$script = "
	<script>
		jQuery(document).ready(function($){
			$( \"form#frn_settings\" ).children('table').children('tbody').children('tr:first').css( \"display\", \"none\" );"; //hides main pool dropdown row
	
	foreach($hide_rows as $row) {
	$script .= '
			$("div#tracking_services table.form-table tr:nth-child('.$row.')").css( "display", "none" ); '; //hides the next set of Infinity rows
	}
	$script .=	"});
	</script>";
	echo $script;
}

function frn_admin_infinity_pools($phones) {
	//called for in add_settings_field layout in admin_submenu_phone.php
	//formats only work if everything removed but numbers and letters
	$format_10 = '/([0-9]..)([0-9]..)([0-9]...)$/'; //assumes numbers don't start with 1
	$format_11 = '/([0-9])([0-9]..)([0-9]..)([0-9]...)$/'; //assumes numbers don't start with 1
	$list_id = "pools";
	?>
	<div id="frn_plugin_infinity_pools" class="frn_help_boxes">
		<h3 style="margin-top:0;">Infinity Pools Help:</h3>
		<ul class="frn_level_1">
			<li>This list is for information purposes only. It is manually updated and the entire system relies on it. </li>
			<li>If pools change in Infinity, the pools array in part_phone_infinity_pools.php will also need to be updated.</li>
			<li>DISC:
				<ul class="frn_level_2">
					<li>These numbers are the ones added to the HTML served by PHP.</li>
					<li>Infinity's system (or any call tracking service) replaces these numbers with the dynamic number for each concurrent user.</li>
					<li>The only other consideration is Google My Business. The primary number we give them must match the discovery number provided in this array.</li>
					<li>The "original" number is the number that we originally used for the respective facilities in our sites. These were all ported to Infinity. In the future, this list of numbers can be changed to correspond with Mitel numbers in case Talkdesk goes down. It would provide a quick option to keep the phones up. If Infinity goes down, all we do is turn off tracking pools and the calls can still be answered.</li>
				</ul>
			</li>
			<li>SHORTCODES: 
				<ul class="frn_level_2">
					<li>All facility shortcodes will be listed in the phone number shortcodes dropdown when editing content.</li>
					<li>To use a facility's pool, you'll need to use the shortcode provided for it. By doing so, if ever we need to deactivate the Infinity system or update it, these shortcodes will automatically revert to the appropriate function--similar to the main frn_phone shortcode.</li>
					<li>The "Infinity" column tells you what kind of number you should see the system switching out for the "discovery number".</li>
					<li>The first column confirms for you the numbers Infinity looks for and what pool it'll switch out with the corresponding pool.</li>
				</ul></li>
			<li>LOCAL INPATIENT: 
				<ul class="frn_level_2">
					<li>Keep in mind that when <b>ph=local</b> is present for a page, it sets a cookie that makes sure all inpatient local numbers are used on all pages of the site instead of the normal toll-free pool.</li>
					<li>If DialogTech is NOT activated, then <b>st-t=local</b> will also trigger the Infinity inpatient local pools.</li>
				</ul></li>
			<li>BD OVERRIDE: 
				<ul class="frn_level_2">
					<li>Use <b>ph=bd</b> to change all facility (including the site's main number) to use our BD pool of numbers. This will change the numbers throughout the website for up to 24 hours for just that visitor.</li>
					<li>This also means that the Call Center will answer the call for FRN instead of the respective facility.</li>
					<li>If it's important to answer by facility, then rely on campaign variables to put your campaign into the business development channel in Marketing reports instead (e.g. utm_campaign=[BD]%20Campaign%20Name).</li>
				</ul></li>
			<li>RESET COOKIES: If you tested the URL var to make sure it works and want to clear your system to work like normal, instead of st-t=local, change to <b>st-t=cancel</b>.</li>
			<li>If you need something changed, contact one of our developers to modify the pools array in the "part_phone_infinity_pools.php" file.</li>
		</ul>
	</div>
	<div class="frn_options_table" style="margin-top:0;">
	<table class="frn_options_table" id="<?=$list_id;?>">
		<tr>
			<th style="width:15px;">ID</th>
			<th style="width:inherit;">Disc: Talkdesk</th>
			<th style="width:inherit;">Disc: Original</th>
			<th style="width:inherit;">SMS</th>
			<th style="width:inherit;">Facility/Brand/Purpose</th>
			<th style="width:inherit;">Type</th>
			<th style="width:inherit;">Infinity</th>
			<th style="width:inherit;">Shortcode</th>
			<th style="width:inherit;"></th>
		</tr>
		<?php
		$infinity_pools="";
		if(function_exists('frn_infinity_pools_list')) {
			$infinity_pools=frn_infinity_pools_list('no_tags'); //get standard list of tag-only pools
		}
		//var_dump($infinity_pools);
		//$total=count($infinity_pools);
		$i=0;
		//$rows=count($infinity_pools);
		if($infinity_pools!=="") {
			//0=TD number, 1=pool name, 2=type, 3=shortcode, 4=keyword for triggers
			foreach($infinity_pools as $key => $pool) {
				$sc="empty";
				if($pool['sc']!=="") $sc='['.$pool['sc'].'_phone]';
				$number = frn_phone_format($pool['number'],"()");
				$mitel  = frn_phone_format($pool['mitel'],"()");
				$sms  = frn_phone_format($pool['sms'],"()");
				$sc='<span class="frn_shortcode_sel">'.$sc.'</span>';
				if(function_exists('frn_table_row_template')) {
					frn_table_row_template($list_id,$i,null,0,array("[".$key."]",$number,$mitel,$sms,$pool['name'],$pool['type'],ucwords($pool['replaced']),$sc)); //table id, row #, row edit option, input fields, array(cell 2, cell 3, etc.)
				}
				$i++;
			}
		}
		?>
	</table><!--
	<br />
	<div>
		<a href="javascript:add_row('<?=$list_id;?>',-1,4,'-');" title="Add another row">Add Row</a>
		<?php // add_row(table_id,id,inputs,edit) ?>
	</div>
	-->
	</div>
	<?php
}

function frn_admin_infinity_tags() {
	//called for in add_settings_field layout in admin_submenu_phone.php
	//formats only work if everything removed but numbers and letters
	$format_10 = '/([0-9]..)([0-9]..)([0-9]...)$/'; //assumes numbers don't start with 1
	$format_11 = '/([0-9])([0-9]..)([0-9]..)([0-9]...)$/'; //assumes numbers don't start with 1
	$list_id = "tags";

	/*
	//CONCEPT:
		to keep it simple for all uses, just use one of two tags: st-t=local & ph=local
		Then this plugin will look at the facility discovery number (or facility pool ID depending on the situation). 
		If "local" is the tag used, it'll switch to the respective facility local pool.
			It'll do that by looking at the core shortcode and search the array for the entry using that plus "_local".
		If we need to be facility specific, we'll need to program one of these options:
		A more specific url tag
		An admin approach that activates/deactivates the feature when "local" is used.
	*/
	?>

	<div>
		<p>
			These local/bd pools can be triggered via the discovery number provided or by url. Add "<b>?ph=local</b>" to the end of your link. If you already have campaign variables in the URL, change the "?" to a "&". If DialogTech is deactivated, "<b>?st-t=local</b>" will still trigger the Infinity inpatient local pools.
			NOTE: When you use a "local" trigger, it'll switch all inpatient facility numbers to their respective local area code, not just the facility you have in mind.
			When "<b>ph=bd</b>" is used, the BD pool replaces all facility numbers listed above with the BD number. That means the call center would no longer know what facility a person is calling in for.
			If that is still important, rely on URL campaign variables instead. Start the campaign name with "<b>[BD]</b> " to have activity reported in the "Business Development" reporting channel.
		</p>

		<div id="frn_plugin_infinity_tags" class="frn_help_boxes">
			<h3 style="margin-top:0;">More Information about the Infinity Pools Triggered by URL Tags:</h3>
			<ul class="frn_level_1">
				<li>This list is for information purposes only.</li>
				<li>If you need something changed, contact one of our developers to modify the pools array in the "part_phone_infinity_pools.php" file.</li>
			</ul>
		</div>
		<div class="frn_options_table">
		<table class="frn_options_table" id="<?=$list_id;?>">
			<tr>
				<th style="width:15px;">ID</th>
				<th style="width:inherit;">Tag</th>
				<th style="width:inherit;">Disc: Talkdesk</th>
				<th style="width:inherit;">Disc: Mitel</th>
				<th style="width:inherit;">Pool Name</th>
				<th style="width:inherit;"></th>
			</tr>
			<?php
			global $infinity_pools;
			$row_i=0; $tags="";
			if(function_exists('frn_infinity_pools_list')) {
				$tags=frn_infinity_pools_list('tags'); //get standard list of tag-only pools
			}
			if(is_array($tags)) {
				foreach($tags as $key => $tag) {
					$number = frn_phone_format($tag['number'],"()");
					$mitel = frn_phone_format($tag['mitel'],"()");
					/*
					//Approach ignored for now. Current assumption is that when local tag used, it will replace all inpatient facilities with local numbers.
					//$assigned=$tag[4]; if($assigned=="all") $selected="selected "; 
																							//tag_array = array(number,pool name,type,shortcode,kw)
					$pool_dropdown='<select id="tag_to_pool" name="phones[infinity][tags]['.$list_id."_".$row_i.'][##i##]">
						<option></option>
						<option value="all" '.$selected.'>Apply to All Pools</option>';
						foreach($infinity_pools as $key=>$pool) {
							$selected="";
							if($key==$assigned) $selected="selected ";
							if(stripos($tag['name'],$pool[4])!==false) $selected="selected ";
							$pool_dropdown.='
							<option value="'.$key.'" '.$selected.'>'.$pool['name']."</option>";
						}
					$pool_dropdown.="
					</select>";	
					*/									//tag,discovery number,pool name, type, pool ID assigned to
					$url_tag="local";
					if($tag['sc']=="bd") $url_tag=$tag['sc'];
					$url_tag='<span class="frn_shortcode_sel">&ph='.$url_tag.'</span>';
					if(function_exists('frn_table_row_template')) {
						frn_table_row_template($list_id,$row_i,'no',0,array("[".$key."]",$url_tag,$number,$mitel,$tag['name']));
					}
					$row_i++;
				}
			}
			?>
		</table>
		</div>

	</div>

	<?php
}



//////////
/// INFINITY POOL MANGEMENT SYSTEM
/////////
/*

OPTION 1: Using custom post type and ACF forms to manage list and capabilities (50% done)
OPTION 2: Using a custom admin approach where the system just updates the array of features instead of using ACF forms (90% done)
OPTION 3: Keep updating the PHP array of options anytime there is a change (in part_phone_infinity_pools.php). (100% done)


OPTION 1 Exploration: Custom Post Type ACF plan (ACF form developed only on Dax multi-dev for FRN.com)
	Postponed until needed
	This is a way to use a custom post type and ACF combo to manage the list and features for Infinity Pools and attributes. 
	Process was not completed since programming the option was still going to be time consuming and possibly unnecessary.
	Keeping code in case we realize it would be helpful and can pick up from here.


//Advanced Custom Fields Admin Options\
		//Discontinued due to it not meeting all Infinity pool management criteria and having a form already built
		//But this is a great option to improve the visual layouts and management of the plugin.
		//Only the dax multidev of FRN.com has this pre-built as an example
		//adds menu item for pools custom post type (won't show in admin otherwise) See commented out section with header INFINITY POOL MANGEMENT SYSTEM in admin_submenu_phone_infinity.php

*/

//add_action('admin_menu', 'frn_pools_submenu');
function frn_pools_submenu() {
	//Called upon in the admin_submenu_phone.php file in order to place menu item in the right order
	$phones = get_option('phones');
	if($phones['tracking']=="infinity") {
		add_submenu_page('frn_features', 'Pools', 'Pools (ACF Test)', 'manage_options', 'edit.php?post_type=pools');
		
		// test for using ACF as an options page for phone settings, but would require more robust infinity pool management that likely needs a custom post type created
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_sub_page(array(
				'page_title' 	=> 'New FRN Phone Settings',
				'menu_title'	=> 'Phones (ACF Test)',
				'parent_slug'	=> 'frn_features',
				//'menu_slug' 	=> 'theme-general-settings',
				//'capability'	=> 'edit_posts',
				//'redirect'		=> false
			));
		}
	}
}

//CONSIDER USING POST TYPE COMBINED WITH ACF OPTIONS TO MANAGE POOLS AND ASSOCIATED DATA
//add_action( 'init', 'frn_admin_infinity_post_type' );
function frn_admin_infinity_post_type() {
	$phones = get_option('phones');
	if($phones['tracking']=="infinity") {
		$labels = array(
			'name'               => 'Pools',
			'singular_name'      => 'Pool',
			'menu_name'          => 'Pools',
			'name_admin_bar'     => 'Pool',
			'add_new'            => 'Add Pool',
			'add_new_item'       => 'Add New Pool',
			'new_item'           => 'New Pool',
			'edit_item'          => 'Edit Pool',
			'view_item'          => 'View Pool',
			'all_items'          => 'All Pools',
			'search_items'       => 'Search Pools',
			'parent_item_colon'  => 'Parent Pools',
			'not_found'          => 'No pools found.',
			'not_found_in_trash' => 'No pools found in trash'
		);

		$args = array(
			'labels'             => $labels,
	        'description'        => 'Description',
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => false,
			//'rewrite'            => array( 'slug' => 'pools' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 10,
			'supports'           => array( 'title', 'editor', 'custom-fields' )
		);

		register_post_type( 'pools', $args );
	}
}






?>