<div id="mobile-f-floating" class="show-for-small-only vert-pad-tiny">
    <div class="row">
      <div class="small-12 columns text-center call">
        <span class="copy">
          Get Started Today  
        </span>
        <span class="telephone"><?php echo do_shortcode('[frn_phone action="Phone Clicks in Mobile Floating Footer"]'); ?></span>
      </div>
    </div>
  </div>
<footer id="f-main" itemscope itemprop="organization" itemtype="http://schema.org/Organization">
  <div class="row branding">
    <div class="medium-4 columns text-center call vert-pad-small">
      Confidential & Private<br>
      <span class="telephone" itemprop="telephone"><?php echo do_shortcode('[frn_phone action="Phone Clicks in Footer"]'); ?></span>
    </div>
    <div class="medium-4 columns">
        <div class="logo-footer text-center">
            <img itemprop="logo" data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/oaks-logo-footer.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/oaks-logo-retina-footer.png, (retina)]" alt="The Oaks at La Paloma">
        </div>
    </div>
    <div class="medium-4 columns vert-pad-small">
      <h3 class="text-center">Our Network</h3>
      <ul class="medium-block-grid-4 small-block-grid-2 network">
        <li class="text-center"><a href="http://blackbearrehab.com/"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/bb-logo.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/bb-retina-logo.png, (retina)]" alt="Black Bear Rehab, Georgia" width="67"></a></li>
        <li class="text-center"><a href="http://www.michaelshouse.com/"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/mh-logo.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/mh-retina-logo.png, (retina)]" alt="Michael's House Treatment Center, Palm Springs, CA" width="66"></a></li>
        <li class="text-center"><a href="http://talbottcampus.com//"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/talbott-recovery-logo.png, (default)]" alt="Talbott Recovery, Atlanta, GA" width="76"></a></li> 
        <li class="text-center"><a href="http://skywoodrecovery.com//"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/skywood-logo.png, (default)]" alt="Skywood Recovery, Augusta, MI" width="76"></a></li> 
        <li class="text-center"><a href="http://www.foundationsrecoverynetwork.com/"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/frn-logo.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/frn-retina-logo.png, (retina)]" alt="Foundations Recovery Network, Nashville, TN" width="76"></a></li> 
      </ul>
    </div>
  </div>

  <div class="row menus">
    <div class="medium-3 columns">
      <h4>About Us</h4>
      <?php wp_nav_menu( array( 'theme_location' => 'footer-1', 'menu_class' => 'no-bullet') ); ?>
    </div>
    <div class="medium-3 columns">
      <h4>Resources</h4>
      <?php wp_nav_menu( array( 'theme_location' => 'footer-2', 'menu_class' => 'no-bullet') ); ?>
    </div>
    <div class="medium-3 columns">
      <h4>Quick Links</h4>
      <?php wp_nav_menu( array( 'theme_location' => 'footer-3', 'menu_class' => 'no-bullet') ); ?>
    </div>
    <div class="medium-3 columns">
      <h4>Contact</h4>
      <?php wp_nav_menu( array( 'theme_location' => 'footer-4', 'menu_class' => 'no-bullet') ); ?>
           <a id="bbblink" class="sehzbas" href="http://www.bbb.org/nashville/business-reviews/drug-abuse-and-addiction-info-and-treatment/foundations-recovery-network-in-brentwood-tn-37038606#bbbseal" title="Foundations Recovery Network, LLC, Drug Abuse & Addiction  Info & Treatment, Brentwood, TN" style="display: block;position: relative;overflow: hidden; width: 100px; height: 38px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-nashville.bbb.org/logo/sehzbas/foundations-recovery-network-37038606.png" width="200" height="38" alt="Foundations Recovery Network, LLC, Drug Abuse & Addiction  Info & Treatment, Brentwood, TN" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2Ffoundations-recovery-network-37038606.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
      <h4>Connect & Share</h4>
      <ul class="social inline-list">
        <li id="facebook"><a href="https://www.facebook.com/lapalomatreatmentcenter"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/fb.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/fb-retina.png, (retina)]" alt="The Oaks Treatment Facebook Page"></a></li>
        <li id="twitter"><a href="https://twitter.com/La_Paloma"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/twt.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/twt-retina.png, (retina)]" alt="The Oaks Twitter Profile"></a></li>
        <li id="linkedin"><a href="https://www.linkedin.com/company/la-paloma-treatment-center/"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/linkedin.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/linkedin.png, (retina)]" alt="The Oaks LinkedIn Profile"></a></li>
        <li id="youtube"><a href="https://www.youtube.com/channel/UC2iEx36aTAm3wVrd4mFsR-A"><img data-interchange="[<?php echo get_template_directory_uri(); ?>/style/images/youtube.png, (default)],[<?php echo get_template_directory_uri(); ?>/style/images/youtube-retina.png, (retina)]" alt="Foundations Recovery Network Youtube Channel"></a></li>
      </ul>
    </div>
  </div>
  <div class="row footer-accreditations">
    <div class="columns medium-6 small-6">
      <script src="https://static.legitscript.com/seals/3417925.js"></script>
    </div>
    <div class="columns medium-6 small-6">
      <img class="naatp-logo" alt="NAATP "src="<?php bloginfo('stylesheet_directory'); ?>/style/images/accreditations/naatp-stacked-white.svg">
    </div>
  </div>
  <div id="copyright" class="text-center">&copy;<?php echo date("Y ");?><span itemprop="name">The Oaks at La Paloma</span> Treatment Center. All Rights Reserved. <a href="<?php echo get_site_url(); ?>/terms">Terms</a> | <a href="<?php echo do_shortcode('[frn_privacy_url]'); ?>">Privacy Policy</a></div>
  <div id="copyright" class="text-center"><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">2009 Lamar Ave</span>, <span itemprop="addressLocality">Memphis, TN</span> <span itemprop="postalCode">38114</span></span> | <a href="https://www.google.com/maps/place/The+Oaks+at+La+Paloma/@35.1125886,-89.998606,17z/data=!3m1!4b1!4m5!3m4!1s0x887f875973a89fab:0x63f3a599c3149029!8m2!3d35.1125886!4d-89.9964173?hl=en-US" >Map It</a></div>
</footer>

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/scripts.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>  

</body> 
</html>
