<?php
//Add Sidebars
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Default Sidebar',
		'id'   => 'default-sidebar',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="text-center">',
		'after_title'   => '</h3>'
	));
}
// if (function_exists('register_sidebar')) {
// 	register_sidebar(array(
// 		'name' => 'Homepage Sidebar',
// 		'description'   => 'These are widgets for the sidebar.',
// 		'before_widget' => '<div id="%1$s" class="widget %2$s">',
// 		'after_widget'  => '</div>',
// 		'before_title'  => '<h2>',
// 		'after_title'   => '</h2>'
// 	));
// }
if (function_exists('register_sidebar')) { 
	register_sidebar(array(
		'name' => 'Blog Sidebar',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="text-center">',
		'after_title'   => '</h3>'
	));
}
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Alumni Page Sidebar',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s box">',  
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	));
}
// if (function_exists('register_sidebar')) {
// 	register_sidebar(array(
// 		'name' => 'Footer',
// 		'description'   => 'These are widgets for the footer.',
// 		'before_widget' => '<div id="%1$s" class="widget %2$s">',
// 		'after_widget'  => '</div>',
// 		'before_title'  => '<h4>',
// 		'after_title'   => '</h4>'
// 	));
// }
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Programs Sidebar',
		'description'   => 'These are sidebar widgets for all pages with the Programs template.',
		'before_widget' => '<div id="%1$s" class="widget %2$s side-list">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="text-center">',
		'after_title'   => '</h3>'
));
}	
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Nashville Outpatient Sidebar',
		'description'   => 'These are sidebar widgets for all pages with the Programs template.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="text-center">',
		'after_title'   => '</h3>'
)); 
}
?>