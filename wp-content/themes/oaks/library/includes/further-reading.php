<?php
  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0');
  if ($children) { ?>
  <div class="side-list">
	  <h3 class="text-center">Further Reading</h3>
	  <ul class="reading small-block-grid-1">
	  <?php echo $children; ?>
	  </ul>
   </div>
<?php } ?>