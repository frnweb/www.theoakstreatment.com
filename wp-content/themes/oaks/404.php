<?php get_header(); ?>
<section role="main">	
	<header id="page-id">
		<div class="row">
			<div class="small-12 columns">
				<h1 class="text-center">We Cannot Find That Page</h1>
				<?php get_template_part('library/includes/breadcrumbs'); ?>
			</div>
		</div>
	</header>		
<article>
	<div class="row">
		<div class="large-8 columns">
			<?php echo do_shortcode('[frn_related html="404"]'); ?>
			<!--<p>We're sorry but there is something wrong with the link you clicked or we no longer have that page on the site. Please check the web address and correct anything you think may need to be in your address bar above.</p>-->
			<p>
				&nbsp;
			</p>
			<p>Our admissions coordinators are available 24 hours a day. If you'd like to ask us a question or the web address is still directing to this page, feel free to contact us or start a live chat and we'll be glad to quickly answer any questions you have (<?php echo do_shortcode('[oaks_phone action="Phone Clicks in 404 Page"]'); ?>).</p>
		</div>
		<?php get_sidebar(); ?>
	</div>	
</article>
</section>
<?php get_footer(); ?>
