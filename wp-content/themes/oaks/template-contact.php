<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>
<section class="banner contact">
	<div class="row">		
		<div class="large-6 large-offset-6 columns text-center vert-pad-large">
			<div class="box-sq-dark horz-marg-small">
				<h1>Contact us</h1>
				<p>If you have questions about whether you or a loved one may need help, please contact us today. Our admissions coordinators are available to provide a confidential assessment and to review the best treatment options for your situation.</p>
				<p>
					<?php echo do_shortcode('[oaks_phone]'); ?>
				</p>
				<div class="text-center">
					<ul class="button-group round stack-for-small">
					  <li><?php echo do_shortcode('[lhn_inpage button="email" text="Email Us" class="button contact-options email-split"]'); ?><!--<a onclick="window.open('https://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160','Ticket','left=' + (screen.width - 550-32) / 2 + ',top=50,scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');return false;" href="https://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160" class="button contact-options">Email Us</a>--></li>
					  <li class="button contact-options chat-split"><?php echo do_shortcode('[lhn_inpage button="chat" id="6926"]'); ?><!--<a onclick="OpenLHNChat();return false;" href="#" class="button contact-options">Live Chat</a>--></li>				
					</ul>
				</div>
			</div>
		</div>		
	</div>
	</section>
</section>

<section>
	<div class="row vert-pad">
		<div class="large-8 columns">
			<h2 class="text-center">Free Assessment</h2>
			<p>Our counselors will guide you through a free, brief and confidential assessment when you call. All calls are completely private and your personal information is 100% confidential. </p>
			<p>The staff of The Oaks is here to listen. If you have questions about the service that we offer or would like more information about our facility, please <?php echo do_shortcode('[oaks_phone text="call" style="color:#718b39"]'); ?> or <?php echo do_shortcode('[lhn_inpage button="email" text="email us"]'); ?> and a representative of The Oaks will contact you shortly. Or chat with a live representative by clicking “<?php echo do_shortcode('[lhn_inpage button="chat" text="Live Chat"]'); ?>”.</p>
			<p>
				<?php echo do_shortcode('[oaks_phone class="frn_contact_assessment"]'); ?>
			</p>
		</div>
		<div class="large-4 columns text-center vert-pad-small">
			<a href="<?php echo get_site_url(); ?>/insurance"><img src="<?php echo get_template_directory_uri(); ?>/style/images/insurance.png" alt="Health insurance providers The Oaks accepts insurance from. Blue Cross Blue Sheild, Cigna, Aetna, Humana, MultiPlan, ValueOptions, and more."></a> 
		</div>
	</div>
</section>

<section class="accred-tier">
	<div class="row vert-pad">
		<div class="large-12 columns text-center">
			<h2>Accredited Members & Supporters of</h2>
			<ul class="inline-list vert-pad-tiny">
				<li><a href="https://www.jointcommission.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/style/images/jcaho-silver.png" alt="JCAHO Logo"></a></li>
			</ul>
		</div>
	</div>
</section>

<section>
	<div class="row vert-pad">
		<div class="large-3 columns">
			<h2 class="underlined">Where we're Located</h2>
		</div>
		<div class="large-6 columns">
			<div class="box">
				<div class="flex-video"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13055.222534194387!2d-89.996663!3d35.111564!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x63f3a599c3149029!2sThe+Oaks+at+La+Paloma!5e0!3m2!1sen!2sus!4v1424204562823" width="600" height="450" frameborder="0" style="border:0"></iframe></div>
			</div>
		</div>
		<div class="large-3 columns vert-pad-xlarge mobile-center" itemscope itemtype="http://schema.org/Organization">
			<h4 itemprop="name">The Oaks at La Paloma</h4>
			<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<div itemprop="streetAddress">2009 Lamar Ave</div>
				<span itemprop="addressLocality">Memphis, TN</span> <span itemprop="postalCode">38114</span>
				<div itemprop="telephone"><?php echo do_shortcode('[oaks_phone]'); ?></div>
				<img class="mobile-hidden" itemprop="logo" alt="The Oaks at La Paloma" src="https://theoakstreatment.com/wp-content/uploads/TheOaks-RoundFINAL-large.png" style="width:100px;" />
			</p>
		</div>
	</div>
</section>
<?php get_footer(); ?>