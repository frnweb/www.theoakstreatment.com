<?php get_header(); ?>
<section role="main">
<header id="page-id">
	<div class="row">
		<div class="small-12 columns">
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			<?php /* If this is a category archive */ if (is_category()) { ?>
				<h1 class="text-center"><?php single_cat_title(); ?> Related Posts</h1>
			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h1 class="text-center">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h1 class="text-center">Archive for <?php the_time('F jS, Y'); ?></h1>
			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h1 class="text-center">Archive for <?php the_time('F, Y'); ?></h1>
			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h1 class="pagetitle text-center">Archive for <?php the_time('Y'); ?></h1>
			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h1 class="pagetitle text-center">Author Archive</h1>
			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h1 class="pagetitle text-center">Blog Archives</h1>
			<?php } ?>
			<?php get_template_part('library/includes/breadcrumbs'); ?>	
		</div>
	</div>	
</header>
	<div class="row">		
			<?php if (have_posts()) :  while  (have_posts()) : the_post(); ?>	
			<article class="blog medium-10 small-centered columns vert-pad-small">
				<div class="box">				
					<div class="row">
						<div class="small-12 columns">			
							<h2 class="text-center"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
						</div>
						<div class="postmeta text-center bottom-marg-xsmall">
							<?php the_time('F jS, Y') ?> | Posted In: <?php the_category(', ') ?>
						</div><!-- end meta -->																																													
						<?php if ( has_post_thumbnail() ) {
					    $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'thumbnail' );					    
					    echo '<div class="medium-4 columns medium-text-right small-text-center">';
					    the_post_thumbnail( 'thumb', array( 'class' => 'th bottom-marg-xsmall' ) );
					    echo '</div>';
					    echo '<div class="large-6 medium-8 columns">';					    
					    echo the_excerpt();					    
					    echo '</div>';
						} else { ?>														
						<div class="small-12 medium-8 medium-centered column">
						<?php the_excerpt(); ?>		
						</div>										
						<?php } ?>										
						<div class="text-center small-12 columns">
							<a href="<?php the_permalink();?>" class="button small round">Keep Reading</a>
						</div>
					</div>	
				</div>
			</article><!-- end blog -->	
			<?php endwhile; endif; ?>	
		<div class="small-12 columns">
			<?php ranklab_pagination();?> 
		</div>
	</div>			
</section> 
<?php get_footer(); ?>