<?php
/*
Template Name: Treatment Options
*/
?>
<?php get_header(); ?>
<section class="banner treatment-options">
	<div class="row vert-pad">
		<div class="large-6 columns text-center vert-pad-large horz-pad-small">
			<h1>Treatment Options</h1>
			<p>Participation in a treatment program is an important first step on the road to recovery. At The Oaks, our rehabilitation program revolves around our client’s readiness to recover and change. We believe that an effective recovery process provides a variety of services aimed at meeting the treatment goals and needs of the people it serves.</p>			
		</div>
		<div class="large-6 columns">
			<div class="video-box horz-marg-small border">
				<div class="flex-video">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/XuA_QpKZTkc" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="under-video">					
					<p class="text-center">What you can expect at The Oaks</p>
				</div>				
			</div>
		</div>
	</div>
	<section class="steps hide-for-small">
		<ul class="medium-block-grid-5">
			<li><a href="<?php echo get_site_url(); ?>/alcohol-rehab">Alcohol Rehab</a></li>
			<li><a href="<?php echo get_site_url(); ?>/drug-rehab">Drug Rehab</a></li>
			<li><a href="<?php echo get_site_url(); ?>/mental-health-treatment">Mental Health</a></li> 
			<li><a href="<?php echo get_site_url(); ?>/dual-diagnosis">Integrated Treatment</a></li>
			<li><a href="<?php echo get_site_url(); ?>/programs/detox">Detoxification</a></li>
		</ul>
	</section>
</section>

<section>
	<div class="row vert-pad">
		<div class="large-9 large-centered columns">
			<h2 class="text-center italic">At The Oaks, we are committed to providing integrated, evidence-based treatment for anyone battling with co-occurring mental health and substance abuse disorders.</h2>
		</div>
	</div>
</section>

<section role="main" class="row">
		<section class="large-9 columns option-boxes">
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Alcohol
						Rehab
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<div class="row">
							<div class="small-12 columns">										
								<p>
									<img class="large-6 medium-4 columns small-10 small-centered medium-uncentered right bottom-marg-xsmall" src="<?php echo get_template_directory_uri(); ?>/style/images/alcohol-rehab.jpg" alt="Alcohol Rehab">
									Our integrated approach to alcohol rehab includes detox, individual and group therapy and aftercare planning. Our alcohol treatment program at The Oaks takes a different approach than most rehab programs. Many drug and alcohol rehab programs offer treatment that deals with addiction but not the other emotional and mental issues that the majority of individuals with substance abuse issues are also facing.
								</p>
							</div>																	 
							<div class="large-6 text-center columns">
								<a href="<?php echo get_site_url(); ?>/alcohol-rehab" class="button small round">Learn More</a>
							</div>
						</div>
					</div>
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Drug
						Rehab
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<div class="row">
							<div class="small-12 columns">
								<p>
									<img class="large-6 medium-4 columns small-10 small-centered medium-uncentered right bottom-marg-xsmall" src="<?php echo get_template_directory_uri(); ?>/style/images/drug-rehab.jpg" alt="Drug Rehab">
									Our specially trained staff members work together with our clients to make important lifestyles changes, manage feelings, develop tools for coping and learn the necessary skills for successful abstinence. By designing effective strategies for prevention and teaching our clients how to identify the warning signs, our goal is to reduce the risk of relapse and help you or your loved one recover.
								</p>
							</div>
							<div class="large-6 text-center columns">
								<a href="<?php echo get_site_url(); ?>/drug-rehab" class="button small round">Learn More</a>
							</div>
						</div>
					</div>
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Mental
						Health
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<div class="row">
							<div class="small-12 columns">
								<p>
									<img class="large-6 medium-4 columns small-10 small-centered medium-uncentered right bottom-marg-xsmall" src="<?php echo get_template_directory_uri(); ?>/style/images/mental-health.jpg" alt="Mental Health">
									Research shows that more than half of the people affected by one condition like an addictive disorder are also affected by at least one other condition like an emotional or mental illness and vice versa. This diagnosis is referred to by a variety of terms including: co-occurring disorder, dual diagnosis, co-morbidity, concurrent disorders and dual disorders. Treatment at The Oaks addresses both the emotional or psychiatric illness and the chemical dependency within a comprehensive program to ensure recovery from both.
								</p>
							</div>
							<div class="large-6 text-center columns">
								<a href="<?php echo get_site_url(); ?>/mental-health-treatment" class="button small round">Learn More</a>
							</div>
						</div>
					</div>
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Integrated
						Treatment
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<div class="row">
							<div class="small-12 columns">
								<p>
									<img class="large-6 medium-4 columns small-10 small-centered medium-uncentered right bottom-marg-xsmall" src="<?php echo get_template_directory_uri(); ?>/style/images/integrated-treatment.jpg" alt="Integrated Treatment">
									The treatment program at The Oaks is nationally recognized for integrative methods and evidence-based practices that have produced proven results for individuals with dual and multiple addiction and mental health disorders. Our methods have been proven more effective than traditional drug and alcohol treatment methods in a five-year research study using the Foundations Treatment Model.
								</p>
							</div>
							<div class="large-6 text-center columns">
								<a href="<?php echo get_site_url(); ?>/dual-diagnosis" class="button small round">Learn More</a>
							</div>
						</div>
					</div>	
				</div>
			</article>
			<article class="row">
				<div class="large-3 columns">
					<h2 class="underlined">Detoxification
					</h2>
				</div>
				<div class="large-9 columns">
					<div class="box">
						<div class="row">
							<div class="small-12 columns">
								<p>
									<img class="large-6 medium-4 columns small-10 small-centered medium-uncentered right bottom-marg-xsmall" src="<?php echo get_template_directory_uri(); ?>/style/images/detoxification.jpg" alt="Detoxification">
									A proper detoxification is the first, and one of the most important steps in the recovery process. Detox may seem like a scary concept for many struggling with drug addiction, but our experienced staff will guide you or your loved one through a medically supervised detox safely and comfortably. Detox usually takes place in a separate wing of the facility, and our medical staff monitors progress every step along the way, making adjustments as necessary. 
								</p>
							</div>
							<div class="large-6 text-center columns">
								<a href="<?php echo get_site_url(); ?>/programs/detox" class="button small round">Learn More</a>
							</div>
						</div>
					</div>
				</div>
			</article>
		</section>
		<aside class="large-3 columns">
			<div class="telephone-cta">
				<p>Get Started Today</p>
				<span class="number"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sidebar"]'); ?></span>
				<div class="text-center">
					<a href="<?php echo get_site_url(); ?>/contact" class="button outline small round vert-marg-tiny">Contact Us</a>
				</div>
			</div>
			<div class="side-list vert-marg-small">
				<h3 class="text-center">Additional Resources</h3>
				<ul>
					<li><a href="<?php echo get_site_url(); ?>/admissions">Admissions</a></li>
					<li><a href="<?php echo get_site_url(); ?>/addiction-therapy">Addiction Therapy</a></li>
					<li><a href="<?php echo get_site_url(); ?>/dual-diagnosis">Dual Diagnosis</a></li>
					<li><a href="<?php echo get_site_url(); ?>/heroin-treatment">Heroin Treatment</a></li>
				</ul>
			</div> 
			<div class="panel"><p>This page contains general information about therapy methods and treatment topics. For more specific information about programs at The Oaks, please visit our <a href="<?php echo get_site_url(); ?>/programs">programs page</a>.</p></div>
		</aside>	
</section>
<?php get_footer(); ?>