<?php get_header(); ?>

<section role="main">
	<header id="page-id">
		<div class="row">
			<div class="small-12 columns">
				<h1 class="text-center"><?php the_title(); ?></h1>
				<?php get_template_part('library/includes/breadcrumbs'); ?>		
			</div>
		</div>	
	</header>
	<div class="row">
		<div class="large-8 columns">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
			<article>				
				<?php the_content(); ?>
			</article>
			<?php endwhile; endif; ?>
		</div>
		<?php get_sidebar(); ?>
		<div class="small-12 columns top-marg-xsmall">
			<?php include(TEMPLATEPATH . "/library/includes/further-reading.php");?>
		</div>
	</div>
</section>
<?php get_footer(); ?>