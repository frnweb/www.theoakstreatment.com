<aside role="complementary" class="large-4 columns">	
<?php if(is_front_page()) {?>
		
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage Sidebar') ) : ?>
	<?php endif; ?>
		
<?php } elseif (is_home() || is_single() || is_archive()) {	?>
	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar') ) : ?>
	<?php endif; ?>
	
<?php } elseif (is_page_template('template-outpatient-main.php') || is_page_template('template-outpatient-single.php')) {	?>
	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Outpatient Sidebar') ) : ?>
	<?php endif; ?>
	
<?php } elseif (is_page_template('template-programs.php')) {	?>
	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Programs Sidebar') ) : ?>
	<?php endif; ?>

<?php } elseif (is_page('alumni' )) {	?>
	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Alumni Page Sidebar') ) : ?>
	<?php endif; ?>	
	
<?php } else { ?>

	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Default Sidebar') ) : ?>
	<?php endif; ?>

<?php } ?> 
</aside> 