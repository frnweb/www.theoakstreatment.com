<?php
/*
Template Name: Blog
*/
?>
<?php get_header(); ?>
<section role="main">
<header id="page-id">
	<div class="row">
		<div class="small-12 columns">
			<h1 class="text-center"><?php the_title(); ?></h1>
			<?php get_template_part('library/includes/breadcrumbs'); ?>	
		</div>
	</div>	
</header>
<div class="row">	
		<?php query_posts('post_type=post&posts_per_page=10&paged='.$paged);?>
		<?php if (have_posts()) : while  (have_posts()) : the_post(); ?>
			<article class="blog medium-10 small-centered columns vert-pad-small">
				<div class="box">				
					<div class="row">
						<div>			
							<h2 class="text-center"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
						</div>
						<div class="postmeta text-center bottom-marg-xsmall">
							<?php the_time('F jS, Y') ?> | Posted In: <?php the_category(', ') ?>
						</div><!-- end meta -->																																													
						<?php if ( has_post_thumbnail() ) {
					    $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'thumbnail' );					    
					    echo '<div class="medium-4 columns medium-text-right small-text-center">';
					    the_post_thumbnail( 'thumb', array( 'class' => 'th bottom-marg-xsmall' ) );
					    echo '</div>';
					    echo '<div class="large-6 medium-8 columns">';
					    echo '<p>';
					    echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);
					    echo '</p>';
					    echo '</div>';
						} else { ?>		
						<p class="text-center">
							<?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true); ?>
						</p>															
						<?php } ?>										
						<div class="text-center small-12 columns">
							<a href="<?php the_permalink();?>" class="button small round">Keep Reading</a>
						</div>
					</div>	
				</div>
			</article><!-- end blog -->	
		<?php endwhile; endif; ?>
		<?php ranklab_pagination();?> 
</div>
</section>
<?php get_footer(); ?>